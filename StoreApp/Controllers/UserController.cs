﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StoreApp.Models;

namespace StoreApp.Controllers
{
    public class UserController : Controller
    {
        UsersEntities db = new UsersEntities();     //商品物件

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult login() {
            return View();
        }

        public ActionResult register(HttpWorkerRequest request) {
            return View();
        }

        public ActionResult ChangePassword() {
            return View();
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }
    }
}