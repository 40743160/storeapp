﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StoreApp.Models;

namespace StoreApp.Controllers
{
    public class HomeController : Controller
    {
        ItemsEntities db = new ItemsEntities();     //商品物件

        /// <summary>
        /// 主頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var items = db.Items.OrderByDescending(m => m.Id).ToList(); //取得所有商品
            return View(items);
        }

        /// <summary>
        /// 商店物品預覽
        /// </summary>
        /// <returns></returns>
        public ActionResult ItemsPreview() {
            var items = db.Items.OrderByDescending(m => m.Id).ToList(); //取得所有商品
            return View(items);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}